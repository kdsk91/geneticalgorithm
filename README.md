# GeneticAlgorithm

The problem is to decide which projects to invest in order to maximize the total return.
You need to develop a genetic algorithm program in Java to solve this problem.
You also need to provide a write up explaining:
-	The encoding of the problem (given in the hints below)
-	The crossover mechanism used
-	The mutation used
-	The fit function used
-	The selection of each generation
-	The stopping criteria

Hints:
Variables: x1, x2, x3 and x4 are binary variables, representing the investment decisions of project 1, 2, 3, and 4. A value 1 indicating invest while a value 0 indicating don’t invest.

Representation: Binary string