import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class ProjectInvestment {

    private static HashSet<String> population = new HashSet<>();
    private static SortedMap<Double, List<String>> fitnessMap = new TreeMap<>(Collections.reverseOrder());
    private static Double mutationProbability = 0.250;
    private static int populationSize = 3;
    private static String fittest;
    private static String secondFittest;
    private static Double previousMaxFitness;


    public ProjectInvestment() {
        for (int i = 0; i < ProjectInvestment.populationSize; ++i)
            ProjectInvestment.generateNewChromosome();
        ProjectInvestment.generateFitnessMap();
        ProjectInvestment.previousMaxFitness = ProjectInvestment.fitnessMap.firstKey();
    }

    public static void main(String[] args) {
        System.out.println("Mutation Probability=" + ProjectInvestment.mutationProbability);
        System.out.println("Population size=" + ProjectInvestment.populationSize);
        ProjectInvestment projectInvestment = new ProjectInvestment();
        int consecutiveGenerationCount = 0;
        int generation = 0;
        while (consecutiveGenerationCount <= 50) {
            System.out.println("\nGeneration " + generation + ":");
            System.out.println("Population: " + ProjectInvestment.population.toString());
            System.out.println("Fitness: " + ProjectInvestment.fitnessMap.toString());
            projectInvestment.selection();
            projectInvestment.crossover();
            projectInvestment.mutate();
            System.out.println("Child 1: " + ProjectInvestment.fittest);
            System.out.println("Child 2: " + ProjectInvestment.secondFittest);
            System.out.println("Fittest Offspring: " + ProjectInvestment.getFittestOffSpring());
            //If some valid offspring is generated and if it's also not already present in population
            if (!ProjectInvestment.getFittestOffSpring().isEmpty() && !ProjectInvestment.population.contains(getFittestOffSpring())) {
                ProjectInvestment.population.remove(ProjectInvestment.getLeastFittestInPopulation());
                ProjectInvestment.population.add(ProjectInvestment.getFittestOffSpring());
                ProjectInvestment.generateFitnessMap();
            }
            NumberFormat formatter = new DecimalFormat("#0.0");
            if (Double.valueOf(formatter.format(ProjectInvestment.previousMaxFitness)) <
                    Double.valueOf(formatter.format(ProjectInvestment.fitnessMap.firstKey()))) {
                ProjectInvestment.previousMaxFitness = ProjectInvestment.fitnessMap.firstKey();
                consecutiveGenerationCount = 0;
            } else {
                consecutiveGenerationCount++;
            }
            generation++;
        }
        System.out.println("\nMost fittest solution to problem is : " + ProjectInvestment.firstFittest());
    }

    public static boolean checkConstraints(int[] input) {
        if (0.5 * input[0] + 1.0 * input[1] + 1.5 * input[2] + 0.1 * input[3] == 0.0) {
            return false;
        } else if (!(0.5 * input[0] + 1.0 * input[1] + 1.5 * input[2] + 0.1 * input[3] <= 3.1)) {
            return false;
        } else if (!(0.3 * input[0] + 0.8 * input[1] + 1.5 * input[2] + 0.4 * input[3] <= 2.5)) {
            return false;
        } else if (!(0.2 * input[0] + 0.2 * input[1] + 0.3 * input[2] + 0.1 * input[3] <= 0.4)) {
            return false;
        }
        return true;
    }

    public static int[] generateNewChromosome() {
        int[] newInput = new int[4];
        newInput[0] = new Random().nextInt(2);
        newInput[1] = new Random().nextInt(2);
        newInput[2] = new Random().nextInt(2);
        newInput[3] = new Random().nextInt(2);
        if (ProjectInvestment.population.contains(arrayToString(newInput)) || (!checkConstraints(newInput))
                || arrayToString(newInput).equals("0000")) {
            newInput = generateNewChromosome();

        }
        ProjectInvestment.population.add(arrayToString(newInput));
        return newInput;
    }

    public static Double calculateFitness(int[] input) {
        NumberFormat formatter = new DecimalFormat("#0.0");
        return Double.valueOf(formatter.format(0.2 * input[0] + 0.3 * input[1] + 0.5 * input[2] + 0.1 * input[3]));
    }

    public static String arrayToString(int[] input) {
        String res = "";
        for (int i = 0; i < input.length; ++i) {
            res += input[i];
        }
        return res;
    }

    public static int[] stringToArray(String chromosome) {
        int[] input = new int[chromosome.length()];
        for (int i = 0; i < input.length; i++) {
            input[i] = Integer.parseInt("" + chromosome.charAt(i));
        }
        return input;
    }

    public void crossover() {
        if(new Random().nextInt(100) + 1 < 75){
            return;
        }
        String parent1 = new String(ProjectInvestment.fittest);
        String parent2 = new String(ProjectInvestment.secondFittest);

        ProjectInvestment.fittest = "" + parent1.charAt(0) + parent2.charAt(1) + parent2.charAt(2) + parent1.charAt(3);
        ProjectInvestment.secondFittest = "" + parent2.charAt(0) + parent1.charAt(1) + parent1.charAt(2) + parent2.charAt(3);
    }

    public void selection() {
        ProjectInvestment.fittest = firstFittest();
        ProjectInvestment.secondFittest = secondFittest();
        System.out.println("Parent 1: " + ProjectInvestment.fittest);
        System.out.println("Parent 2: " + ProjectInvestment.secondFittest);
    }

    public void mutate() {
        String res = "";
        if (ProjectInvestment.mutationProbability <= 0.150) {
            //Do nothing
        } else if (ProjectInvestment.mutationProbability <= 0.650) {
            //change one gene at random index
            int index = new Random().nextInt(4);
            for (int i = 0; i < ProjectInvestment.fittest.length(); i++) {
                if (i == index) {
                    res += ProjectInvestment.reverseGene(ProjectInvestment.fittest.charAt(i));
                } else {
                    res += ProjectInvestment.fittest.charAt(i);
                }
            }
            ProjectInvestment.fittest = res;
            res = "";
            //change one gene at random index
            index = new Random().nextInt(4);
            for (int i = 0; i < ProjectInvestment.secondFittest.length(); i++) {
                if (i == index) {
                    res += ProjectInvestment.reverseGene(ProjectInvestment.secondFittest.charAt(i));
                } else {
                    res += ProjectInvestment.secondFittest.charAt(i);
                }
            }
            ProjectInvestment.secondFittest = res;
        } else if (ProjectInvestment.mutationProbability <= 0.850) {
            //change two genes at random indices
            int index1 = new Random().nextInt(4);
            int index2 = new Random().nextInt(4);
            while (index1 == index2) {
                index2 = new Random().nextInt(4);
            }
            for (int i = 0; i < ProjectInvestment.fittest.length(); i++) {
                if (i == index1 || i == index2) {
                    res += ProjectInvestment.reverseGene(ProjectInvestment.fittest.charAt(i));
                } else {
                    res += ProjectInvestment.fittest.charAt(i);
                }
            }
            ProjectInvestment.fittest = res;
            res = "";
            //change two genes at random indices
            index1 = new Random().nextInt(4);
            index2 = new Random().nextInt(4);
            while (index1 == index2) {
                index2 = new Random().nextInt(4);
            }
            for (int i = 0; i < ProjectInvestment.secondFittest.length(); i++) {
                if (i == index1 || i == index2) {
                    res += ProjectInvestment.reverseGene(ProjectInvestment.secondFittest.charAt(i));
                } else {
                    res += ProjectInvestment.secondFittest.charAt(i);
                }
            }
            ProjectInvestment.secondFittest = res;
        } else if (ProjectInvestment.mutationProbability < 1.000) {
            //change 3 genes except one at random index
            int index = new Random().nextInt(4);
            for (int i = 0; i < ProjectInvestment.fittest.length(); i++) {
                if (i != index) {
                    res += ProjectInvestment.reverseGene(ProjectInvestment.fittest.charAt(i));
                } else {
                    res += ProjectInvestment.fittest.charAt(i);
                }
            }
            ProjectInvestment.fittest = res;
            res = "";
            //change 3 genes except one at random index
            index = new Random().nextInt(4);
            for (int i = 0; i < ProjectInvestment.secondFittest.length(); i++) {
                if (i != index) {
                    res += ProjectInvestment.reverseGene(ProjectInvestment.secondFittest.charAt(i));
                } else {
                    res += ProjectInvestment.secondFittest.charAt(i);
                }
            }
            ProjectInvestment.secondFittest = res;
        } else {
            //change all genes
            for (int i = 0; i < ProjectInvestment.fittest.length(); i++) {
                res += ProjectInvestment.reverseGene(ProjectInvestment.fittest.charAt(i));
            }
            ProjectInvestment.fittest = res;
            res = "";
            //change all genes
            for (int i = 0; i < ProjectInvestment.secondFittest.length(); i++) {
                res += ProjectInvestment.reverseGene(ProjectInvestment.secondFittest.charAt(i));
            }
            ProjectInvestment.secondFittest = res;
        }

        if (!checkConstraints(stringToArray(ProjectInvestment.fittest))) {
            //If generated children does not fulfil constraints, then reject it
            ProjectInvestment.fittest = "";
        }
        if (!checkConstraints(stringToArray(ProjectInvestment.secondFittest))) {
            //If generated children does not fulfil constraints, then reject it
            ProjectInvestment.secondFittest = "";
        }
    }

    public static String getFittestOffSpring() {
        if (ProjectInvestment.fittest.isEmpty()) {
            return ProjectInvestment.secondFittest;
        } else if (ProjectInvestment.secondFittest.isEmpty()) {
            return ProjectInvestment.fittest;
        }
        return calculateFitness(stringToArray(ProjectInvestment.fittest)) >
                calculateFitness(stringToArray(ProjectInvestment.secondFittest)) ? ProjectInvestment.fittest :
                ProjectInvestment.secondFittest;
    }

    public static String getLeastFittestInPopulation() {
        String last = "";
        for (String str : ProjectInvestment.fitnessMap.get(ProjectInvestment.fitnessMap.lastKey())) {
            last = str;
        }
        return last;
    }

    public static char reverseGene(char c) {
        if (c == '0')
            return '1';
        else
            return '0';
    }

    public static void generateFitnessMap() {
        ProjectInvestment.fitnessMap.clear();
        for (String str : ProjectInvestment.population) {
            List<String> list;
            if (ProjectInvestment.fitnessMap.get(calculateFitness(stringToArray(str))) == null) {
                list = new ArrayList<>();
                list.add(str);
                ProjectInvestment.fitnessMap.put(calculateFitness(stringToArray(str)), list);
            } else {
                if (!ProjectInvestment.fitnessMap.get(calculateFitness(stringToArray(str))).contains(str))
                    ProjectInvestment.fitnessMap.get(calculateFitness(stringToArray(str))).add(str);
            }
        }
    }

    public static String firstFittest() {
        return ProjectInvestment.fitnessMap.get(fitnessMap.firstKey()).get(0);
    }

    public static String secondFittest() {
        if (ProjectInvestment.fitnessMap.get(fitnessMap.firstKey()).size() > 1) {
            return ProjectInvestment.fitnessMap.get(fitnessMap.firstKey()).get(1);
        }
        int count = 0;
        String res = "";
        for (List<String> input : Arrays.asList(ProjectInvestment.fitnessMap.values()).iterator().next()) {
            count++;
            if (count == 2) {
                res = input.get(0);
                break;
            }
        }
        return res;
    }
}
